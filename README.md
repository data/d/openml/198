# OpenML dataset: delta_elevators

https://www.openml.org/d/198

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: Rui Camacho (rcamacho@garfield.fe.up.pt)  
**Source**: [Regression datasets collection Luis Torgo](http://www.dcc.fc.up.pt/~ltorgo/Regression/DataSets.html)  
**Please cite**:   

This data set is also obtained from the task of controlling the ailerons of a F16 aircraft, although the target variable and attributes are different from the ailerons domain. The target variable here is a variation instead of an absolute value, and there was some pre-selection of the attributes.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/198) of an [OpenML dataset](https://www.openml.org/d/198). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/198/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/198/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/198/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

